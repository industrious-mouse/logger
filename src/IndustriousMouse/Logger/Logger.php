<?php namespace IndustriousMouse\Logger;

use Config;

/**
 * Class Logger
 * @package IndustriousMouse\Logger
 */
class Logger
{
	/**
	 * API Version
	 */
	const VERSION = 0.1;

	/**
	 * Report levels
	 *
	 * @var array
	 */
	private $levels = [
		'emergency',
		'alert',
		'critical',
		'error',
		'warning',
		'notice',
		'info',
		'debug'
	];

	/**
	 * Config array
	 *
	 * @var array
	 */
	private $config = [];

	/**
	 * Last CURL Response
	 *
	 * @var array
	 */
	private $last_curl_response = [];

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->config = Config::get('logger::api');
	}

	/**
	 * @param $method
	 * @param $arguments
	 *
	 * @return bool
	 */
	public function __call($method, $arguments)
	{
		if(substr($method, 0, 3) == 'log')
		{
			$level = substr(strtolower($method), 3);

			if(in_array($level, $this->levels))
			{
				$message = (isset($arguments[0]))
					? $arguments[0]
					: '';

				$data = (isset($arguments[1]))
					? $arguments[1]
					: [];

				$tags = (isset($arguments[2]))
					? $arguments[2]
					: [];

				return $this->send($level, $message, $data, $tags);
			}
		}
	}

	/**
	 * Sends the message
	 *
	 * @param $level
	 * @param $message
	 * @param $data
	 * @param $tags
	 *
	 * @return bool
	 */
	private function send($level, $message, $data, $tags)
	{
		$info = [
			'timestamp' => date('c'),
			'log' => [
				'version' => self::VERSION
			],
			'user' => [
				'level' => $level,
				'message' => $message,
				'data' => $data,
				'tags' => $tags,
				'environment' => \App::environment()
			],
			'env' => [
				'get' => $_GET,
				'post' => $_POST,
				'server' => $_SERVER,
				'php_version' => phpversion()
			]
		];

		$username = $this->config['username'];
		$password = $this->config['password'];

		// Lets make the data tiny!
		$data = base64_encode(gzcompress(json_encode($this->object_to_array($info))));

		// Send via CURL
		$curl = curl_init($this->config['url']);

		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, compact('data', 'username', 'password'));
		curl_setopt($curl, CURLOPT_VERBOSE, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($curl, CURLOPT_TIMEOUT, 5);

		$curl_response = @curl_exec($curl);
		$curl_response_info = @curl_getinfo($curl);

		@curl_close($curl);

		$response = [
			'response' => $curl_response,
			'success'  => ($curl_response_info['http_code'] == '200'),
			'info'     => $curl_response_info
		];

		$this->setLastCurlResponse($response);

		return $this;
	}

	/**
	 * Translate object into array
	 *
	 * @param $data
	 *
	 * @return array
	 */
	private function object_to_array($data)
	{
		if(is_array($data) || is_object($data))
		{
			$result = [];

			foreach ($data as $key => $value)
			{
				$result[$key] = $this->object_to_array($value);
			}

			return $result;
		}

		return $data;
	}

	/**
	 * Sets the CURL Response
	 *
	 * @param array $last_curl_response
	 */
	private function setLastCurlResponse($last_curl_response)
	{
		$this->last_curl_response = $last_curl_response;
	}



	/**
	 * Returns the last CURL Response
	 *
	 * @return array
	 */
	public function getLastCurlResponse()
	{
		return $this->last_curl_response;
	}
}